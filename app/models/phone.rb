class Phone < ApplicationRecord
	has_many :phone_details
	belongs_to :brand
	has_attached_file :image, styles: { medium: "160x212>", thumb: "100x100>" }
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	after_create :add_phone_details
	def to_param
  		alias_name
	end

	def add_phone_details
		self.phone_details.build().save
	end
end
