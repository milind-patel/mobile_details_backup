class ArticlesController < ApplicationController
  before_action :find_post, only: [:edit, :update, :show, :delete]
  # Index action to render all posts
  def index
    @posts = Article.all
  end

  # New action for creating post
  def new
    @post = Article.new
  end

  # Create action saves the post into database
  def create
    @post = Article.new(article_params)
    if @post.save
      flash[:notice] = "Successfully created article!"
      redirect_to article_path(@post)
    else
      flash[:alert] = "Error creating new article!"
      render :new
    end
  end

  # Edit action retrives the post and renders the edit page
  def edit
    @post = Article.find(params[:id])
  end

  # Update action updates the post with the new information
  def update
    if @post.update_attributes(article_params)
      flash[:notice] = "Successfully updated article!"
      redirect_to article_path(@post)
    else
      flash[:alert] = "Error updating article!"
      render :edit
    end
  end

  # The show action renders the individual post after retrieving the the id
  def show
    @post = Article.find(params[:id])
  end

  # The destroy action removes the post permanently from the database
  def destroy
    if @post.destroy
      flash[:notice] = "Successfully deleted post!"
      redirect_to posts_path
    else
      flash[:alert] = "Error updating post!"
    end
  end

  private

  def article_params
    params.fetch(:article).permit(:title, :body)
  end

  def find_post
    @post = Article.find(params[:id])
  end
end