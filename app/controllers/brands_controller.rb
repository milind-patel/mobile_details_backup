class BrandsController < ApplicationController
  before_action :set_brand, only: [:edit, :update, :destroy]

  # GET /brands
  # GET /brands.json
  def index
    @brands = Brand.all.order("priority ASC")
  end

  # GET /brands/1
  # GET /brands/1.json
  def show
    case params[:alias_name]
    when "phones"
      @phones = Phone.all.order("brand_id ASC")
      render :template => 'phones/index'
    when "phone_details"
      @phone_details = PhoneDetail.all.order("brand_id ASC")
      render :template => 'phone_details/index'
    when "blog"
      render :template => 'articles/index'
    else
      @brand = Brand.find_by(alias_name: params[:alias_name])
      @phones = @brand.phones.order("release_date DESC")
      @latest_releases = @phones.order("release_date ASC").limit(4)
    end
  end

  # GET /brands/new
  def new
    @brand = Brand.new
  end

  # GET /brands/1/edit
  def edit

  end

  # POST /brands
  # POST /brands.json
  def create
    @brand = Brand.new(brand_params)
    @brand.alias_name = @brand.name
    respond_to do |format|
      if @brand.save
        format.html { redirect_to @brand, notice: 'Brand was successfully created.' }
        format.json { render :show, status: :created, location: @brand }
      else
        format.html { render :new }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /brands/1
  # PATCH/PUT /brands/1.json
  def update
    params[:brand][:alias_name] = params[:brand][:name]
    respond_to do |format|
      if @brand.update(brand_params)
        Brand.update_priority(@brand)
        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { render :show, status: :ok, location: @brand }
      else
        format.html { render :edit }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand.destroy
    respond_to do |format|
      format.html { redirect_to brands_url, notice: 'Brand was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.find_by(alias_name: params[:alias_name])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.fetch(:brand).permit(:image,:logo,:name,:description,:alias_name,:priority)
    end
end
