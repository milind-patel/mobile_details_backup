class AddBrandLogoToBrand < ActiveRecord::Migration[5.0]
  def change
    add_attachment :brands, :logo
    add_attachment :brands, :image
  end
end
