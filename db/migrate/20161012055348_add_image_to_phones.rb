class AddImageToPhones < ActiveRecord::Migration[5.0]
  def change
    add_attachment :phones, :image
  end
end
