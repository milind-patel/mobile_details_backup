class AddDisplaySizeToPhoneDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :phone_details, :display_size, :string
    add_column :phone_details, :display_resolution, :string
    add_column :phone_details, :display_type, :string
    add_column :phone_details, :display_multitouch, :boolean
    
    add_column :phone_details, :ram, :string
    add_column :phone_details, :chipset, :string
    add_column :phone_details, :battery_capacity, :string
    add_column :phone_details, :battery_type,:string

    add_column :phone_details, :technology, :string
    add_column :phone_details, "2g_bands", :string
    add_column :phone_details, "3g_bands", :string
    add_column :phone_details, "4g_bands", :string
    add_column :phone_details, :gprs, :string
    add_column :phone_details, :edge, :string

    add_column :phone_details, :announced, :string
    add_column :phone_details, :status, :string
    add_column :phone_details, :dimensions, :string
    add_column :phone_details, :weight, :string
    add_column :phone_details, :sim, :string


    add_column :phone_details, :os, :string
    add_column :phone_details, :cpu_type, :string
    add_column :phone_details, :cpu_speed, :string
    add_column :phone_details, :gpu, :string
    add_column :phone_details, :card_slot, :string
    add_column :phone_details, :rom, :string
    
		add_column :phone_details, :primary_camera, :string
    add_column :phone_details, :camera_features, :string
    add_column :phone_details, :video, :string
    add_column :phone_details, :secondary_camera, :string

    add_column :phone_details, :alert_types, :string
    add_column :phone_details, :loud_speaker, :boolean
    add_column :phone_details, "3.5mm_jack", :string
    add_column :phone_details, :wlan, :string

    add_column :phone_details, :bluetooth, :string
    add_column :phone_details, :gps, :string
    add_column :phone_details, :nfc, :boolean
    add_column :phone_details, :sensors, :string

    add_column :phone_details, :messaging, :string
    add_column :phone_details, :browser, :string
    add_column :phone_details, :java, :boolean,default: false
    add_column :phone_details, :colors, :string
    add_column :phone_details, :price_in_rupees, :float
    add_column :phone_details, :price_in_dollar, :float
    add_column :phone_details, :protection,:string
    add_column :phone_details, :speed,:string



    # add_column :phones, :camera_pixel, :string ,default: "8MP"
    # add_column :phones, :ram, :string, default: "2GB"
    # add_column :phones, :battery, :string, default: "3500mAH"
    # add_column :phones, :os_version, :string, default: "v6.0.1"
    # add_column :phones, :os_type, :string, default: "Android"
    # add_column :phones, :storage, :string, default: "16GB"
    # add_column :phones, :display_size, :string, default: "5.0"
    # add_column :phones, :display_resolution, :string, default: "720x1280"
    # add_column :phones, :weight, :string, default: "153.6gm"
    # add_column :phones, :thickness, :string, default: "9.6mm"
    # add_column :phones, :release_date, :string, default: Time.now.to_date
    # add_column :phones, :chipset, :string, default: "MT6735P"
    # add_column :phones, :video_pixel, :string,default: "720p"
    # add_column :phones, :battery_technology, :string,default: "Li-Ion"
  end
end
