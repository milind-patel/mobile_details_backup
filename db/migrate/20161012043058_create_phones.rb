class CreatePhones < ActiveRecord::Migration[5.0]
  def change
    create_table :phones do |t|
    	t.string :name
    	t.string :alias_name
    	t.integer :brand_id
    	t.datetime :release_date
     	t.timestamps
    end
    add_index :phones,:brand_id
  end
end
