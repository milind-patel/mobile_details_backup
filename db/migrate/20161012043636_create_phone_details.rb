class CreatePhoneDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :phone_details do |t|
    	t.integer :phone_id
    	t.integer :brand_id
    	t.timestamps
    end
    add_index :phone_details, :phone_id
    add_index :phone_details, :brand_id
  end
end
