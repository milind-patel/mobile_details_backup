# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161019212424) do

  create_table "articles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "brands", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "alias_name"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "priority"
    t.text     "description",        limit: 65535
  end

  create_table "ckeditor_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree
  end

  create_table "phone_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "phone_id"
    t.integer  "brand_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "display_size"
    t.string   "display_resolution"
    t.string   "display_type"
    t.boolean  "display_multitouch"
    t.string   "ram"
    t.string   "chipset"
    t.string   "battery_capacity"
    t.string   "battery_technology"
    t.string   "battery_type"
    t.string   "technology"
    t.string   "2g_bands"
    t.string   "3g_bands"
    t.string   "4g_bands"
    t.string   "gprs"
    t.string   "edge"
    t.string   "announced"
    t.string   "status"
    t.string   "dimensions"
    t.string   "weight"
    t.string   "sim"
    t.string   "os"
    t.string   "cpu"
    t.string   "gpu"
    t.string   "card_slot"
    t.string   "internal_memory"
    t.string   "primary_camera"
    t.string   "camera_features"
    t.string   "video"
    t.string   "secondary_camera"
    t.string   "alert_types"
    t.boolean  "loud_speaker"
    t.string   "3.5mm_jack"
    t.string   "wlan"
    t.string   "bluetooth"
    t.string   "gps"
    t.boolean  "nfc"
    t.string   "sensors"
    t.string   "messaging"
    t.string   "browser"
    t.boolean  "java",                          default: false
    t.string   "colors"
    t.float    "price_in_rupees",    limit: 24
    t.float    "price_in_dollar",    limit: 24
    t.string   "protection"
    t.string   "speed"
    t.index ["brand_id"], name: "index_phone_details_on_brand_id", using: :btree
    t.index ["phone_id"], name: "index_phone_details_on_phone_id", using: :btree
  end

  create_table "phones", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "alias_name"
    t.integer  "brand_id"
    t.datetime "release_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.index ["brand_id"], name: "index_phones_on_brand_id", using: :btree
  end

end
