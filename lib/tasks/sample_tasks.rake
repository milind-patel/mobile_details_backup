namespace :sample_tasks do
	desc "Reorder Priority"
	task reorder_priority: :environment do
		first_brand = Brand.find_by(name: "Apple")
		all_brands = Brand.all
		first_array = all_brands.collect{|t| t.id if t.id >= first_brand.id}.reject{|t| t.nil?}
		first_array.each_with_index do |val,index|
			brand = Brand.find(val)
			brand.update_attributes(priority: index + 1)
		end
		remaining_array = Brand.all.where(priority: nil)
		remaining_array.each_with_index do |val,index|
			brand = Brand.find(val)
			brand.update_attributes(priority: first_array.size + index + 1)
		end
	end
end