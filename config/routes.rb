Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  get 'home/index1'
  root "home#index1"

  resources :brands,param: :alias_name do
    member do
      #get ""
    end
  end
  get ':alias_name/:phone_alias_name' => "phones#show"
  get ':alias_name' => "brands#show"
  resources :phone_details
  resources :phones,param: :alias_name
  resources :articles,path: 'blog'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
